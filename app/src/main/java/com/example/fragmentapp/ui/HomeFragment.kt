package com.example.fragmentapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.fragmentapp.R
import com.example.fragmentapp.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Add button listener to send you to another fragment
        binding.cvBlue.setOnClickListener{
            findNavController().navigate(R.id.action_homeFragment_to_blueFragment)
        }
        binding.cvRed.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_redFragment)
        }
        binding.cvGreen.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_greenFragment)
        }
        binding.cvGray.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_grayFragment)
        }
        binding.cvPurple.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_purpleFragment)
        }
        binding.cvYellow.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_yellowFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

}